const TWO = 2;

let word = prompt('Enter one word: ', '');
let lengthOfWord = word.length;
let spacelength = word.replace(/[^ ]/g, '').length;

if (!word || lengthOfWord === spacelength) {
    alert('Invalid value');
} else {
    if (lengthOfWord % TWO !== 0) {
        let letter = Math.floor(lengthOfWord / TWO);
        alert(word[letter]);
    }

    if (lengthOfWord % TWO === 0) {
        let firstLetter = Math.floor(lengthOfWord / TWO - 1);
        let lastLetter = firstLetter + 1;

        if (word[firstLetter] === word[lastLetter]) {
            alert('Middle characters are the same');
        } else {
            alert(word.slice(firstLetter, lastLetter + 1));
        }
    }
}
