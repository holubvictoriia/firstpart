function visitLink(path) {

    let countClick = 0;

    if (path === 'Page1') {
        countClick++;
    localStorage.setItem('clickedPage1', countClick);
    }
    if (path === 'Page2') {
        countClick++;
        localStorage.setItem('clickedPage2', countClick);
    }
    if (path === 'Page3') {
        countClick++;
        localStorage.setItem('clickedPage3', countClick);
    }

}



function viewResults() {

    let arr = [];

    let page1 = localStorage.getItem('clickedPage1');
    let page2 = localStorage.getItem('clickedPage2');
    let page3 = localStorage.getItem('clickedPage3');


    let str1 = `You visited Page1 ${page1} time(s)`;
    let str2 = `You visited Page2 ${page2} time(s)`;
    let str3 = `You visited Page3 ${page3} time(s)`;

    arr.push(str1, str2, str3);


    let content = document.getElementById('content');
    let listInfo = document.createElement('ul');

    for (let i = 0; i < arr.length; i++) {

        listInfo.innerHTML += `<li>${arr[i]}</li>`;
        content.appendChild(listInfo);
    }

    localStorage.clear()
}
