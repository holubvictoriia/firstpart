function inputMathOperations() {
    let input = prompt('Enter math expression:');
    let expression = input.split(' ').join('');
    return expression;
}


function showResult() {
    let result = eval(inputMathOperations());

    if (isNaN(result) || !isFinite(result)) {
        throw new Error('Invalid operation');
    }
    alert(result);
}
try {
    showResult();
} catch (error) {
    alert(error.message);
    inputMathOperations();
}

