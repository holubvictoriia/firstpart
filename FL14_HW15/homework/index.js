/* START TASK 1: Your code goes here */

let cells = document.getElementsByTagName('td');

for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', changeColor);
}

function changeColor() {
    this.style.backgroundColor= 'yellow';
}

/* END TASK 1 */

/* START TASK 2: Your code goes here */

function sendPhoneNumber() {
    let text = document.getElementById('inp').value;
    let y = document.getElementById('not_allowed');
    let input = document.querySelector('input')
    let reg = /^\+380[0-9]{9}$/;
    if (reg.test(text)) {
        let x = document.getElementById('allowed');
        x.style.display = 'block';
        y.style.display = 'none';
        input.style.borderColor = 'black';
    } else {
        y.style.display = 'block';
        input.value = '';
        input.style.borderColor = 'rgb(255, 44, 44)';
        input.placeholder = 'Type phone number in format +380*********';
        document.querySelector('button').disabled = true;
    }
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */

let court = document.getElementById('court');
let ball = document.getElementById('ball');
const TWO = 2;

court.addEventListener('click', getClickPosition, false);


function getClickPosition(e) {
    let parentPosition = getPosition(court);
    let xPosition = e.clientX - parentPosition.x - ball.clientWidth / TWO;
    let yPostion = e.clientY - parentPosition.y - ball.clientHeight / TWO;

    ball.style.left = xPosition + 'px';
    ball.style.top = yPostion + 'px';
}

function getPosition(elem) {
    let xPosition = 0;
    let yPosition = 0;

    while (elem) {
        if (elem.tagName === 'BODY') {
            let xScroll = elem.scrollLeft || document.documentElement.scrollLeft;
            let yScroll = elem.scrollTop || document.documentElement.scrollTop;

            xPosition += elem.offsetLeft - xScroll + elem.clientLeft;
            yPosition += elem.offsetTop - yScroll + elem.clientTop;
        } else {
            xPosition += elem.offsetLeft - elem.scrollLeft + elem.clientLeft;
            yPosition += elem.offsetTop - elem.scrollTop + elem.clientTop;
        }
        elem = elem.offsetParent;
    }
    return {
        x: xPosition,
        y: yPosition
    }
}


let backgroundA = document.getElementById('backboardA');

backgroundA.addEventListener('click', addPointA);

function addPointA() {
    let teamB = +document.getElementById('teamB').innerText;
    teamB += 1;
    document.getElementById('teamB').innerText = teamB;
}

let backgroundB = document.getElementById('backboardB');

backgroundB.addEventListener('click', addPointB);

function addPointB() {
    let teamA = +document.getElementById('teamA').innerText;
    teamA += 1;
    document.getElementById('teamA').innerText = teamA;
}

/* END TASK 3 */
