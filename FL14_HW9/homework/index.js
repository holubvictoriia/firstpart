const TEN = 10;

//1
function convert(arr) {
    let nArr = [];
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'number') {
            nArr.push(arr[i].toString());
        } else if (typeof arr[i] === 'string') {
            nArr.push(parseInt(arr[i]))
        }
    }
    return nArr
}


//2
function executeForEach(arr, cb) {
    for (let i = 0; i < arr.length; i++) {
        arr[i] = cb(arr[i])
    }
}


//3
function mapArray(arr, cb) {
    for (let i = 0; i < arr.length; i++) {
        arr[i] = cb(+arr[i])
        }
}

//4
function filterArray(arr, cb) {
    let nArr = [];

    for (let i = 0; i < arr.length; i++) {
        if(cb(arr[i])) {
            nArr.push(arr[i])
        }
    }
    return nArr
}

//5
function getValuePosition(arr, value) {
    let nValue;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            // console.log(i+1)
            nValue = i+1
            break;
        }
        if (arr[i] !== value) {
            nValue = false
        }
    }
    return nValue;
}


//6
function flipOver(str) {
    let nStr = '';
    for (let i = str.length-1; i >= 0; i--) {
        nStr += str[i];
    }
    return nStr
}


//8
function getArrayOfKeys(fruits, name) {
    let nArr = [];
    for (let i = 0; i < fruits.length; i++) {
        nArr.push(fruits[i].name);
    }
    console.log(nArr);
}


//9
function getTotalWeight(basket) {
    let sumOfWeight = 0;
    for (let i = 0; i < basket.length; i++) {
        sumOfWeight += basket[i].weight;
    }
    return sumOfWeight;
}


//10
function getPastDay(date, day) {
    date.setDate(date.getDate() - day);
    console.log(date)
}


//11
function formatDate(data) {
    let today = data;
    let d = today.getDate();
    let m = today.getMonth();
    let y = today.getFullYear();
    let h = today.getHours();
    let min = today.getMinutes();
    if (d < TEN) {
        d = '0' + d;
    }
    if (m < TEN) {
        m = '0' + m;
    }
    if (h < TEN) {
        h = '0' + h;
    }
    if (min < TEN) {
        min = '0' + min;
    }
    console.log(y + '/' + m + '/' + d + ' ' + h + ':' + min)
}
