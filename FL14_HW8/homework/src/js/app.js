const questions_list = JSON.parse(localStorage.getItem('questions'));

const WINNER_SUM = 1000000;
const TWO = 2;
const NUM_100 = 100;

let startBtn = document.getElementById('startBtn');
let qstn = document.getElementsByClassName('question')[0];
let answers = document.getElementsByClassName('answer');
let skipQuestion = document.getElementById('skip-guestion');
let prizeForRound = document.getElementById('prize-for-round');
let totalPrize = document.getElementById('total-prize');


startBtn.addEventListener('click', startGame);

function getRandomInt(num) {
    return Math.floor(Math.random() * Math.floor(num));
}


let totalSum = 0;
let prizeSum = 100;


function startGame() {
    totalSum = 0;
    prizeSum = NUM_100;
    prizeForRound.innerHTML = prizeSum;
        totalPrize.innerHTML = totalSum;

    document.getElementById('main').style.display = 'block';
    skipQuestion.style.display = 'block';

    chooseQuestion();

}

let numb = questions_list.length;
let questionNum;
let showedQuestions = [];


function winning() {
    if (totalSum >= WINNER_SUM) {
        document.getElementById('main').style.display = 'none';
        skipQuestion.style.display = 'none';
        let finalLine = document.createElement('p');
        finalLine.className = 'final-line';
        finalLine.innerHTML = `Congratulations! You won: ${totalSum}`;
        document.getElementById('game').append(finalLine);
    }
}



function chooseQuestion() {
    winning();
    questionNum = getRandomInt(numb);

    let reslt = showedQuestions.some((val) => {
        return val === questionNum
    });

    if (!reslt) {
        showedQuestions.push(questionNum);
    } else {
        chooseQuestion()
    }


    qstn.innerHTML = questions_list[questionNum].question;
   
    for (let i = 0; i < answers.length; i++) {
        answers[i].innerHTML = questions_list[questionNum].content[i]
    }   
}



for (let i = 0; i < answers.length; i++) {
   answers[i].addEventListener('click', chooseAnswer);


 function chooseAnswer() {
     if (i === questions_list[questionNum].correct) {
         totalSum += prizeSum;
         prizeSum *= TWO;
         prizeForRound.innerHTML = prizeSum;
         totalPrize.innerHTML = totalSum;
     } else {
         document.getElementById('main').style.display = 'none';
         skipQuestion.style.display = 'none';
         let finalLine = document.createElement('p');
         finalLine.className = 'final-line';
         finalLine.innerHTML = `Game over. Your prize is: ${totalSum}`;
         document.getElementById('game').append(finalLine);
     }
     chooseQuestion();
 }
}


skipQuestion.addEventListener('click', disableBtn);

function disableBtn() {
    chooseQuestion();
    skipQuestion.setAttribute('disabled', 'true');
    skipQuestion.style.backgroundColor = 'white';
}
