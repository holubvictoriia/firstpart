const TREE = 3;

//1
function isEquals(value1, value2) {
    return value1 === value2;
}


//2
function numberToString(value) {
    return value.toString()
}


//3
function storeNames() {
    let arr = [];

    for (let i = 0; i < arguments.length; i++) {
        arr.push(arguments[i])
    }

    return arr
}


//4
function getDivision(num1, num2) {
    if (num1 > num2) {
        return num1 / num2
    } else {
        return num2 / num1
    }
}


//5
function negativeCount(arr) {
    let countOfNegtiveNum = 0;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            countOfNegtiveNum++
        }
    }
    return countOfNegtiveNum
}


//6
function letterCount(string, letter) {
    let countOfLetter = 0;
    for (let lttr of string) {
        if (lttr === letter) {
            countOfLetter++
        }
    }
    return countOfLetter
}


//7
function countPoints(arr) {
    let count = 0;
    let x;
    let y;
    let symb;
    for (let i = 0; i < arr.length; i++) {
        symb = arr[i].indexOf(':');
        x = +arr[i].slice(0, symb);
        y = +arr[i].slice(symb+1);
        if (x > y) {
            count+=TREE
        } else if (x === y) {
            count+=1
        }
    }
    return count
}

