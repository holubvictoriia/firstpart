const DATA = 1970;

//1
function getAge(birthdayDate) {
    let difMounth = Date.now() - birthdayDate.getTime();
    let ageDif = new Date(difMounth);

    return Math.abs(ageDif.getUTCFullYear() - DATA)
}


//2
function getWeekDay(dt) {
    let daysList = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    return daysList[dt.getDay()];
}


//5
function isValidIdentifier(str) {
    let res = /(?=.{1,})(?=^\D)(?=.[^!?@#%&* ]+$)/
    return res.test(str)
}


//6
function capitalize(str) {
    return str.replace(/^[a-z]|\s\w/g, (u) => u.toUpperCase());
}


//7
function isValidAudioFile(str) {
    let res = /([a-zA-Z])+(.mp3|.flac|.alac|.aac)$/
    return res.test(str)

}

//8
function getHexadecimalColors(color) {
    let res = /#[a-f|A-F|0-9]+/g
    return color.match(res)
}

//9  //Done 100
function isValidPassword(str) {
    let res = /(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.{8,})/
    return res.test(str)

}


//10
function addThousandsSeparators() {
    let str = '1234567890';
    let reg = /\d{3}/g

    return str.match(reg)
}
