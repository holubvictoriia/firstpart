const root = document.getElementById('root');
//first section


let localLet = window.storage.globalLet;


let section1 = document.createElement('section');
let section2 = document.createElement('section');

section1.setAttribute('id', 'section1');
section2.setAttribute('id', 'section2');

root.appendChild(section1);
root.appendChild(section2);

let ulEl = document.createElement('ul');

section1.appendChild(ulEl);


for (let i = 0; i < localLet.length; i++) {
    let li = document.createElement('li');
    li.innerText = localLet[i].name;
    ulEl.appendChild(li);

}

ulEl.addEventListener('click', showOverview)

let btnAdd = document.createElement('button');
btnAdd.appendChild(document.createTextNode('Add book'));
section1.appendChild(btnAdd);



//second section

let img = new Image();
let div = document.createElement('div');
let div2 = document.createElement('div');
let h3 = document.createElement('h3');
let p = document.createElement('p');


function showOverview(event) {

        if (event.target && event.target.nodeName === 'LI') {

        for (let i = 0; i < localLet.length; i++) {
            if (event.target.innerText === localLet[i].name) {

                div.setAttribute('id', 'picture');
                div2.setAttribute('id', 'content');

                img.src = localLet[i].photo;
                h3.innerText = localLet[i].name + ' - ' + localLet[i].author;
                p.innerText = localLet[i].plot;

                div.appendChild(img);
                section2.appendChild(div);

                div2.appendChild(h3);
                div2.appendChild(p);
                section2.appendChild(div2);
            }
        }
    }
}


