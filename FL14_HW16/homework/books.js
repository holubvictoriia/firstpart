let books = [
     {
        name: 'Mere Christianity', author: 'C. S. Lewis',
        photo:`https://play-lh.googleusercontent.com/77Spdsw9DxRqgAn6CbF-fFEgSgwrvbSTZTo20_DNi`+
            `43-5XYX6hVjfqfOe5HtSEgykTox8xstkmDqQA`,
        plot: 'Mere Christianity is a book that explains Christianity' +
            'to those who might be skeptical, offering a philosophical approach '+
            'to understanding basic Christian doctrines. Lewis begins by arguing '+
            'against atheism by pointing out what he calls the Law of Human Nature, '+
            'or a universal, absolute truth.'
    },
     {
        name: 'Gone With the Wind', author: 'Margaret Mitchell',
        photo: 'https://upload.wikimedia.org/wikipedia/en/6/6b/Gone_with_the_Wind_cover.jpg',
        plot: 'Gone with the Wind is a novel by American writer Margaret Mitchell, '+
            'first published in 1936. The story is set in Clayton County and Atlanta, '+
            'both in Georgia, during the American Civil War and Reconstruction Era. ' +
            'It depicts the struggles of young Scarlett O\'Hara, the spoiled daughter of' +
            ' a well-to-do plantation owner, who must use every means at her disposal to' +
            ' claw her way out of poverty following Sherman\'s destructive "March to the Sea".' +
            ' This historical novel features a coming-of-age story, with the title' +
            ' taken from the poem “Non Sum Qualis eram Bonae Sub Regno ' +
            'Cynarae”, written by Ernest Dowson.'
    },
     {
        name: 'War and Peace', author: 'Leo Tolstoy',
        photo: `https://kbimages1-a.akamaihd.net/6faafc7b-cec8-4c6d-9b3d-42ae`+
            `ba8fc09a/1200/1200/False/war-and-peace-166.jpg`,
        plot: 'War and Peace broadly focuses on Napoleon’s invasion  of Russia in' +
            ' 1812 and follows three of the most well-known characters in literature: ' +
            'Pierre Bezukhov, the illegitimate son of a count who is fighting for ' +
            'his inheritance and yearning for spiritual fulfillment; Prince Andrei ' +
            'Bolkonsky, who leaves his family behind to fight in the war against' +
            ' Napoleon; and Natasha Rostov, the beautiful young daughter of a nobleman ' +
            'who intrigues both men.'
    },
     {
        name: 'The Chronicles of Narnia', author: 'C. S. Lewis',
        photo: 'https://m.media-amazon.com/images/M/MV5BMTc0NTUwMTU5OV5BMl5BanBnXkFtZTcwNjAwNzQzMw@@._V1_.jpg',
        plot: ' The series is set in the fictional realm of Narnia, a ' +
            'fantasy world of magic, mythical beasts, and talking animals. ' +
            'It narrates the adventures of various children who play central roles ' +
            'in the unfolding history of the Narnian world. Except in The Horse and ' +
            'His Boy, the protagonists are all children from the real world who are ' +
            'magically transported to Narnia, where they are sometimes called upon ' +
            'by the lion Aslan to protect Narnia from evil. The books span the ' +
            'entire history of Narnia, from its creation in The Magician\'s Nephew ' +
            'to its eventual destruction in The Last Battle.'
    },
     {
        name: 'The Hunger Games', author: 'Suzanne Collins',
        photo: 'https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1586722975l/2767052.jpg',
        plot: 'The Hunger Games is a 2008 dystopian novel by the American writer ' +
            'Suzanne Collins. It is written in the voice of 16-year-old Katniss ' +
            'Everdeen, who lives in the future, post-apocalyptic nation of Panem in ' +
            'North America. The Capitol, a highly advanced metropolis, ' +
            'exercises political control over the rest of the nation. The ' +
            'Hunger Games is an annual event in which one boy and one girl aged ' +
            '12–18 from each of the twelve districts surrounding the Capitol are ' +
            'selected by lottery to compete in a televised battle royale to the death.'
    },
     {
        name: 'The Brothers Karamazov', author: 'Fyodor Dostoyevsky',
        photo: 'https://images-na.ssl-images-amazon.com/images/I/81++6NdCdhL.jpg',
        plot: 'Ryevsk, Russia, 1870. Tensions abound in the Karamazov family. ' +
            'Fyodor is a wealthy libertine who holds his purse strings tightly. ' +
            'His four grown sons include Dmitri, the eldest, an elegant officer, ' +
            'always broke and at odds with his father, betrothed to Katya, herself ' +
            'lovely and rich. The other brothers include a sterile aesthete, a ' +
            'factotum who is a bastard, and a monk. Family tensions erupt when ' +
            'Dmitri falls in love with one of his father\'s mistresses, the coquette ' +
            'Grushenka. Two brothers see Dmitri\'s jealousy of their father as an ' +
            'opportunity to inherit sooner. Acts of violence lead to the story\'s ' +
            'conclusion: trials of honor, conscience, forgiveness, and redemption.'
    },
     {
        name: 'Pride and Prejudice', author: 'Jane Austen',
        photo: 'https://m.media-amazon.com/images/I/51tiK-eB3JL.jpg',
        plot: 'Pride and Prejudice follows the turbulent relationship between ' +
            'Elizabeth Bennet, the daughter of a country gentleman, and Fitzwilliam ' +
            'Darcy, a rich aristocratic landowner. They must overcome the titular sins ' +
            'of pride and prejudice in order to fall in love and marry.'
    },
    {
        name: 'To Kill a Mockingbird', author: 'Harper Lee',
        photo: 'https://images-na.ssl-images-amazon.com/images/I/51N5qVjuKAL._SX309_BO1,204,203,200_.jpg',
        plot: 'To Kill a Mockingbird is a novel by Harper Lee. Although it was ' +
            'written in 1960 it is set in the mid-1930s in the small town of Maycomb, ' +
            'Alabama. It is narrated by Scout Finch, a six-year-old tomboy who ' +
            'lives with her lawyer father Atticus and her ten-year-old brother Jem.'
    },
     {
        name: 'A Passage to India', author: 'E. M. Forster',
        photo: 'https://images-na.ssl-images-amazon.com/images/I/51zb3NjYwwL._SY445_.jpg',
        plot: 'Adela Quested is sailing from England to British Raj India ' +
            'with Mrs Moore, the latter the mother of her intended bridegroom, ' +
            'Ronny Heaslop; Mrs Moore\'s son from her first marriage. He is the ' +
            'City magistrate in Chandrapore, the anglicised spelling of Chandrapur. ' +
            'Adela intends to see if she can make a go of it.'
    },
    {
        name: 'The Old Man and the Sea', author: 'Ernest Hemingway',
        photo: 'https://images-na.ssl-images-amazon.com/images/I/81ik3zWcceL.jpg',
        plot: 'The story centres on an aging fisherman who engages in an epic ' +
            'battle to catch a giant marlin. Ernest Hemingway (right) with Joe ' +
            'Russell (raising a glass), an unidentified young man, and a marlin, ' +
            'Havana Harbor, 1932.'
    }
];

window.storage = {};
window.storage.globalLet = books;
